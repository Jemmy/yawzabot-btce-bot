//
//  BtceApiKit.h
//  BtceApiKit
//
//  Created by Maxim Shmotin on 01/04/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for BtceApiKit.
FOUNDATION_EXPORT double BtceApiKitVersionNumber;

//! Project version string for BtceApiKit.
FOUNDATION_EXPORT const unsigned char BtceApiKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <BtceApiKit/PublicHeader.h>


