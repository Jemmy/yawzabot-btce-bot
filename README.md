# Yawza Bot version 0.972 #

### List of Changes ###

### 0.972 ###

* Add support for iOS 9

### App Store ###

Check it out: [https://itunes.apple.com/app/id977348233](https://itunes.apple.com/app/id977348233)

### Author ###

* Planemo Studio
* Russia, Saint-Petersburg 
* version 0.972

### Description ###

A cryptocurrency trading bot (Light Client) for BTC-e.com writing in Swift. Make and cancel orders from your smartphone wherever you are.
All Btc-e trade pairs supported (not only Bitcoin)
Today Widget + Apple Watch Support.
It has BTC-e.com public and private API built in.

### Screenshots ###

![IMG_0659.PNG](https://bitbucket.org/repo/rnd8q6/images/3968740502-IMG_0659.PNG)
![IMG_0660.PNG](https://bitbucket.org/repo/rnd8q6/images/3505750122-IMG_0660.PNG)
![IMG_0662.PNG](https://bitbucket.org/repo/rnd8q6/images/735227056-IMG_0662.PNG)
![IMG_0661.PNG](https://bitbucket.org/repo/rnd8q6/images/3314967878-IMG_0661.PNG)
![IMG_0482.PNG](https://bitbucket.org/repo/rnd8q6/images/974512553-IMG_0482.PNG)


![iOS Simulator Screen Shot - Apple Watch 02 Apr 2015 00.39.14.png](https://bitbucket.org/repo/rnd8q6/images/2921907119-iOS%20Simulator%20Screen%20Shot%20-%20Apple%20Watch%2002%20Apr%202015%2000.39.14.png)
![iOS Simulator Screen Shot - Apple Watch 02 Apr 2015 00.39.31.png](https://bitbucket.org/repo/rnd8q6/images/2055264353-iOS%20Simulator%20Screen%20Shot%20-%20Apple%20Watch%2002%20Apr%202015%2000.39.31.png)

### Configuration ###

Config.plist

```
#!swift

    api_key = "YOUR_API_KEY"
    secret_key = "YOUR_SECRET_KEY"
```

Or it can be configured in app Settings.

### Contacts ###

max@planemostd.com

Supports Btc-e Public API v3, Trade API v1. iOS 8+ & Touch ID Auth

Under GPL v3 License