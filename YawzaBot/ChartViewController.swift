//
//  ChartViewController.swift
//  YawzaBot
//
//  Created by Maxim Shmotin on 16/03/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit
import WebKit

class ChartViewController: UIViewController, UIWebViewDelegate {
    @IBOutlet weak var webView: UIWebView!
    let chartUrl: String = "http://www.coindesk.com/price/"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (Reachability.isConnectedToNetwork()){
            loadWebViewChart()
        }else{
            showNoConnectionDialog()
        }
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        SVProgressHUD.dismiss()
    }
    
    func webViewDidStartLoad(webView: UIWebView){
        SVProgressHUD.showWithStatus("Loading chart")
    }
    
    func webViewDidFinishLoad(webView: UIWebView) {
        SVProgressHUD.dismiss()
    }
    
    func loadWebViewChart(){
        let url = NSURL(string: chartUrl)
        let req = NSURLRequest(URL:url!)
        self.webView!.loadRequest(req)
    }
    
    func showNoConnectionDialog(){
        let dialog = UIAlertController(title: "No Internet Connection", message: "Check your network!", preferredStyle: UIAlertControllerStyle.Alert)
        let cancelAction = UIAlertAction(title: "Fine", style: .Default, handler: nil)
        dialog.addAction(cancelAction)
        self.presentViewController(dialog, animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
