//
//  Double+Extensions.swift
//  YawzaBot
//
//  Created by Maxim Shmotin on 04/04/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

extension Double {
    func format() -> String {
        let decimalPlaces: String = ".3"
        return NSString(format: "%\(decimalPlaces)f", self) as String
    }
    
    func getTotalWithFee(withValue: Double) -> Double{
        let BTCE_FEE: Double = 0.002
        return (self * withValue)*(1.0 + BTCE_FEE)
    }
}