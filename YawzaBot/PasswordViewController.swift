//
//  MainScreenViewController.swift
//  YawzaBot
//
//  Created by Maxim Shmotin on 17/11/14.
//  Copyright (c) 2014 Planemo. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet var passwordTextField: UITextField!
    @IBOutlet var apiKeyTextField: UITextField!
    @IBOutlet var secretKeyTextField: UITextField!
    @IBOutlet var testDataLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        recognizeTestData()
        passwordTextField.delegate = self
        
        //NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillAppear:", name: UIKeyboardWillShowNotification, object: nil)
        //NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillHide:", name: UIKeyboardWillHideNotification, object: nil)
    }
    
    private func keyboardWillAppear(notification: NSNotification) {
        let info = notification.userInfo!
        let size = (info[UIKeyboardFrameBeginUserInfoKey] as! NSValue).CGRectValue()
        let endSize = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).CGRectValue()
        let height = max(size.height, endSize.height)
        let origin = endSize.minY
        var edgeInsets: UIEdgeInsets?
        if let responder = UIResponder.currentFirstResponder() as? UIView {
            let windowSpaceFrame = responder.convertRect(responder.bounds, toView: nil)
            let offset = windowSpaceFrame.maxY - origin + 25
            if offset > 0 {
                edgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: offset, right: 0)
            }
            
        } else {
            edgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: height + 25, right: 0)
        }
        if let _ = edgeInsets {
            //scrollView.contentInset = insets
            //scrollView.scrollIndicatorInsets = insets
        }
    }
    
    private func keyboardWillHide(notification: NSNotification) {
        let info = notification.userInfo!
        let duration = (info[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber).doubleValue
        var curve = UIViewAnimationCurve.EaseInOut
        (info[UIKeyboardAnimationCurveUserInfoKey] as! NSNumber).getValue(&curve)
        let contentInsets = UIEdgeInsetsZero
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(duration)
        UIView.setAnimationCurve(curve)
        //scrollView.contentInset = contentInsets;
        //scrollView.scrollIndicatorInsets = contentInsets;
        UIView.commitAnimations()
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    private let API_KEY_LENGTH: Int = 40
    @IBAction func save(sender: UIButton){
        if (apiKeyTextField.text!.isEmpty || apiKeyTextField.text?.length < 40 ||
            secretKeyTextField.text!.isEmpty ){
            self.showEnterPasswordDialog()
        }else{
            Authentication.setUserPassword(passwordTextField.text!)
            Authentication.apiKey = apiKeyTextField.text
            Authentication.secretKey = secretKeyTextField.text
            BtceApiHandler.sharedInstanse.setupInitValues()
            self.dismissViewControllerAnimated(true, completion: nil)
        }
    }
    
    func recognizeTestData(){
        if (BtceApiHandler.sharedInstanse.isConfigDataUsed()){
            testDataLabel.text = "Test Data Enabled"
        }else{
            testDataLabel.text = "Test Data Disabled"
        }
    }
    
    @IBAction func close(sender: UIButton){
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    private func showEnterPasswordDialog(){
        let alertController = UIAlertController(title: "Warning", message: "Enter valid ApiKey & SecretKey first or press 'Close'", preferredStyle: .Alert)
        let cancelAction = UIAlertAction(title: "Okay", style: .Cancel, handler: nil)
        alertController.addAction(cancelAction)
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}
