//
//  ViewController.swift
//  YawzaBot
//
//  Created by Maxim Shmotin on 12/11/14.
//  Copyright (c) 2014 Planemo. All rights reserved.
//

import UIKit
import Foundation

class TradeViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet var balanceLabel: UILabel!
    @IBOutlet var btcToUsdSellRateLabel: UILabel!
    @IBOutlet var btcToUsdBuyRateLabel: UILabel!
    @IBOutlet var amountCurrencyLabel: UILabel!
    @IBOutlet var pricePerCurrencyLabel: UILabel!

    @IBOutlet var quickBuySellBtn: UIButton!
    @IBOutlet var rateTextField: UITextField!
    @IBOutlet var amountTextField: UITextField!
    @IBOutlet var totalAmountLabel: UILabel!
    
    private var currentRateSell: Double = 0.0
    private var currentRateBuy: Double = 0.0
    private var openOrders: [Int] = []
    private var textFieldText: Int?
    
    private var tracker: NSTimer!
    
    internal var tradePair: TradePair!
    
    private struct NewOrder{
        static var type =  Order.OrderType.BUY
        static var rawType: String {
            get{
                return type.rawValue.capitalizedString
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        amountCurrencyLabel.text = "Amount \(((tradePair.askCurrencyTitle()).uppercaseString))"
        pricePerCurrencyLabel.text = "Price per \(((tradePair.askCurrencyTitle()).uppercaseString))"
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        tracker = NSTimer.scheduledTimerWithTimeInterval(2.0, target: self, selector: Selector("update"), userInfo: nil, repeats: true)
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        tracker.invalidate()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
        
    @IBAction func createNewOrder(sender: UIButton){
        if (rateTextField.text != "0" || amountTextField.text != "0"){
            BtceApiHandler.sharedInstanse.buySellCurrency(tradePair!.code, rate: rateTextField.text!, amount: amountTextField.text!,
                action: NewOrder.type ,completionHandler: { (success) in
                    dispatch_async(dispatch_get_main_queue()) {
                        self.rateTextField.text = ""
                        self.amountTextField.text = ""
                        if (success){
                            Dialog.show(Dialog.Title.SUCCESS, message: "\(Order.OrderType.SELL)")
                        }else{
                            Dialog.show(Dialog.Title.WARNING, message: "Order hasn't been created")
                        }
                    }
            })
        }
    }

    // Bot Logic
    
    func update() {
        if (Reachability.isConnectedToNetwork()){
            self.updateCurrencyRate()
            self.updateAccountInfo()
        }else{
            Dialog.show(Dialog.Title.ERROR, message: "Status: The Internet connection appears to be offline")
        }
    }
    
    func updateCurrencyRate(){
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)){ //
            if let json = BtceApiHandler.sharedInstanse.getTradePairRate(self.tradePair.code){
                let ticker: AnyObject! = json[self.tradePair.code]
                let buy = ticker["buy"] as! Double
                let sell = ticker["sell"] as! Double

                dispatch_async(dispatch_get_main_queue()) {
                    self.btcToUsdSellRateLabel.text = "Last Bid: \(sell)"
                    self.btcToUsdBuyRateLabel.text = "Last Ask: \(buy)"
                    self.currentRateSell = sell
                    self.currentRateBuy = buy
                }
            }
        }
    }
    
    func updateAccountInfo(){
        dispatch_async(BtceApiHandler.sharedInstanse.dispatch_serial_queue){
            if let json = BtceApiHandler.sharedInstanse.updateAccountInfo(){
                if let info = json["return"] as? NSDictionary{
                    let funds = info["funds"] as! NSDictionary
                    let firstCurrency = funds[self.tradePair.askCurrencyTitle()] as! Double
                    let secCurrency = funds[self.tradePair.bidCurrencyTitle()] as! Double
                    dispatch_async(dispatch_get_main_queue()) {
                        self.balanceLabel.text = "Balance: \(firstCurrency.format()) \(self.tradePair.askCurrencyTitle().uppercaseString) | \(secCurrency.format()) \(self.tradePair.bidCurrencyTitle().uppercaseString)"
                    }
                }else if let error = json["error"] as? String{
                    dispatch_async(dispatch_get_main_queue()) {
                        self.tracker.invalidate()
                        self.quickBuySellBtn.hidden = true
                        Dialog.show(Dialog.Title.ERROR, message: error.capitalizeFirst)
                        SVProgressHUD.dismiss()
                    }
                }
            }
        }
    }
        
    func textFieldDidEndEditing(textField: UITextField) {
        let rate = Double.init(rateTextField.text!)
        let amount = Double.init(amountTextField.text!)
        totalAmountLabel.text = "\(rate!.getTotalWithFee(amount!)) \((tradePair.bidCurrencyTitle().uppercaseString))"
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    @IBAction func switchBuySellStates(sender: UISegmentedControl){
        NewOrder.type = (sender.selectedSegmentIndex == 1) ? Order.OrderType.SELL : Order.OrderType.BUY
        self.quickBuySellBtn.setTitle(NewOrder.rawType, forState: UIControlState.Normal)
    }
    
    @IBAction func done(sender: UIButton){
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
}

